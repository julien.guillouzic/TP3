// Port a ecouter
var port = 1234;

// Modules a utiliser
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var cookieparser = require('cookie-parser');
var app = express();
var db = new sqlite3.Database('db.sqlite');
var path = require('path');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(cookieparser());

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {

		res.json(data);
	});
});

function generate_token(length){
	// fonction generant un cookie
	var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
	var b = [];

	for (var i=0; i<length; i++) 
		{
			var j = (Math.random() * (a.length-1)).toFixed(0);
			b[i] = a[j];
		}
	return b.join("");
}



app.post("/login", function(req, res, next) {

	//récuperation du username et pwd
	var ident = req.body.login; 
	var pwd = req.body.pswd; 

	// test verification login password
	db.all('SELECT ident, password FROM users WHERE ident=? AND password=?;', [ident, pwd], function(err, data) 
	{
		if(data.length>0) 
		{
			// login et password valide dans la base

			//déclaration et recuperation token,ident
			var token = generate_token(32); 
			console.log(token);
			//déclaration cookie

	         // recherche session active 
			db.all('SELECT * FROM sessions WHERE ident=?;', [ident], function (err,data) 
			{	
				
				if(data.length>0)
				{	
					// utilisateur a une session active

					db.all('UPDATE sessions SET token=? WHERE ident=?;',[token, ident], function(err, data) 
					{
						// envoi du resultat
						//res.cookie('KeySession', token);
						//res.json({ status: true, token: token });
						
					});
    			}
				else
				{	
					// utilisateur n'a pas de session active
					db.all('INSERT INTO sessions (ident, token) VALUES (?, ?);', [ident, token], function(err, data) 
					{ 
						// envoie du resultat
						//res.json({ status: true, token: token });

					});
				}
			});

			res.cookie('KeySession', token);

			//console.log(token);

			res.redirect('/');


		}
		else
		{
			// login et password non existant dans la base
			res.json({ status: false, token: null });
		}
	});
});

app.post('/logout', function(req, res, next)
{
	var cookie = req.cookies.KeySession;

	db.run('DELETE FROM sessions WHERE token = ?;', [cookie]);

	res.redirect('/')
	
});

app.get('/',function(req, res, next)
{
	var cookie = req.cookies.KeySession;
	
	db.all('SELECT * FROM sessions WHERE token=?;', [cookie], function (err,data)
	{
		if(data.length > 0 )
		{
			res.sendFile(path.join(__dirname,'public','logout.html'));
		}
		else
		{
			res.sendFile(path.join(__dirname,'public','index.html'));
		}
	});

});
app.use(express.static('public'));



app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});


